/* See LICENSE file for copyright and license details. */
#include <X11/XF86keysym.h>
#define TERM "st -e "
#define SUDOTERM "sudo st -e "
// Yes there is a trailing space in both TERM and SUDOTERM
/* SUDOTERM usable only if you have a way for dmenu to ask password for root commands
or if your user can invoke sudo commands without password (sudoers) */

/* How to make dmenu ask for password:

- have this dmenu patch https://tools.suckless.org/dmenu/patches/password/
- make a shell (!/bin/sh) script called askpass
- script content: "dmenu -P -p "$1" <&- && printf &"
- chmod +x askpass, then put it in your $PATH
- append "export SUDO_ASKPASS="path/to/askpass"" to your ~/.bashrc (or /etc/bash/bashrc if Artix/Void or /etc/bash.bashrc if any other Linux)
- shit will then work
*/

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int gappx     = 8;        /* gaps between windows */
static const unsigned int snap      = 20;       /* snap pixel */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = {
    "Droid:pixelsize=12:antialias=true:autohint=true",
    "Joypixels:pixelsize=11:antialias=true:autohint=true",
    "NotoColorEmoji:pixelsize=10:antialias=true:autohint=true",
    "Font Awesome:size=11:antialias=true"
};

//static const char green[]       = "#39D015";
//static const char yellow[]      = "#fbb901";
//static const char tux_feet[]    = "#f8cb32";
//static const char orange2[]     = "#f7c379";
//static const char red[]         = "#8b0000";
//static const char orange[]      = "#ff8700"; // the better orange
//static const char purple[]      = "#63028c"; // bg

static const char cream[]       = "#fae3b0";
static const char bar[]        = "#232232";
static const char black[]       = "#0f0f0f";
static const char glass[]       = "#23282d";
static const char grey[]        = "#bfc6d4";
static const char purple[]      = "#d700ff"; // fg
static const char white[]       = "#edf2fc";
static const char *colors[][3]      = {

	/*               fg     bg     border   */
	[SchemeNorm] = { white,bar,bar }, // comment this to make unsel window border transparent
	[SchemeSel]  = { grey,bar,cream },
	[SchemeStatus]  = { grey,bar,black}, // Statusbar right
	[SchemeTagsSel]  = { bar,grey,grey}, // Tagbar selected
    [SchemeTagsNorm]  = { grey,bar,grey}, // Tagbar unselected
    [SchemeInfoSel]  = { grey,bar,bar}, // infobar mid selected
    [SchemeInfoNorm]  = { grey,bar,bar}, // infobar mid unselected*/
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
 	/* class        instance    title			tags mask	isfloating  isterminal  noswallow  monitor */
 	{ "TradingView",    NULL,   NULL,			1 << 2,		    0,		    0,          0,      -1 },
	{ "St",             NULL,   NULL,           0,              0,          1,          0,      -1 },
 	{ "st-256color",    NULL,   NULL,			0,		        0,	    	1,          0,      -1 },
	{ NULL,             NULL,   "Event Tester", 0,              0,          0,          1,      -1 }, /* xev */
 	{ "Telegram",       NULL,   NULL,			1 << 2,		    0,	    	0,	        0,      -1 },
};

/* layout(s) */
static const float mfact     = 0.26; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
//	{ "><>",      NULL },    /* no layout function means floating behavior */
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/dash", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-i", "-p", "Select program" };

static Key keys[] = {
/* Default section */
	/* modifier		key	    	function    	argument */
	{ MODKEY,		XK_p,       spawn,          {.v = dmenucmd } },
    { MODKEY,		XK_grave,   spawn,          SHCMD("st") },

	{ MODKEY,		XK_j,		focusstack,     {.i = +1 } },
	{ MODKEY,		XK_k,		focusstack,     {.i = -1 } },
	{ MODKEY,		XK_i,		incnmaster,     {.i = +1 } },
	{ MODKEY,		XK_d,		incnmaster,     {.i = -1 } },

// m/smfact
	{ MODKEY,               XK_h,       setmfact,       {.f = -0.04} },
	{ MODKEY,               XK_l,       setmfact,       {.f = +0.04} },
    { MODKEY|ShiftMask,     XK_l,       setcfact,       {.f = +0.20} },
	{ MODKEY|ShiftMask,     XK_h,       setcfact,       {.f = -0.20} },
	{ MODKEY|ShiftMask,     XK_o,       setcfact,       {.f =  0.00} },

	{ MODKEY,		XK_Return,  zoom,       {0} },
//	{ MODKEY,		XK_Tab,     view,       {0} },
	{ MODKEY,		XK_q,       killclient,	{0} },
	{ MODKEY|ShiftMask,     XK_b,       togglebar,  {0} },

// Layout
//    { MODKEY,		    XK_t,		setlayout,      {.v = &layouts[0]} },
//	{ MODKEY,		    XK_f,		setlayout,      {.v = &layouts[1]} },
//	{ MODKEY|ShiftMask,	XK_space,	togglefloating, {0} },
//

/* Multi monitor options */
	{ MODKEY,	    	XK_comma,       focusmon,	{.i = -1 } },
	{ MODKEY,		    XK_period,      focusmon,	{.i = +1 } },
	{ MODKEY|ShiftMask,	XK_comma,   tagmon,		{.i = -1 } },
	{ MODKEY|ShiftMask,	XK_period,	tagmon,		{.i = +1 } },

/* Tag keys */
	TAGKEYS(		    XK_1,		0)
	TAGKEYS(	    	XK_2,		1)
	TAGKEYS(		    XK_3,		2)
	TAGKEYS(		    XK_4,		3)
	TAGKEYS(	    	XK_5,		4)

// Capslock

  { 0,			XK_Caps_Lock,    spawn,		SHCMD("xdotool key Caps_Lock; pkill -RTMIN+1 dwmblocks") },

/* Media */


  // Toggle audio source
  { MODKEY,     XK_t,       spawn,		SHCMD("pipeswitch") },

    // Volume
  { MODKEY,     XK_equal,                   spawn,		SHCMD("wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+; pkill -RTMIN+4 dwmblocks") },
  { 0,			XF86XK_AudioRaiseVolume,    spawn,		SHCMD("wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+; pkill -RTMIN+4 dwmblocks") },

  { MODKEY,     XK_minus,                   spawn,		SHCMD("wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-; pkill -RTMIN+4 dwmblocks") },
  { 0,			XF86XK_AudioLowerVolume,    spawn,		SHCMD("wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-; pkill -RTMIN+4 dwmblocks") },

  { 0,			XF86XK_AudioMute,           spawn,		SHCMD("wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle; pkill -RTMIN+4 dwmblocks") }, // mute volume


  { 0,          XF86XK_AudioNext,           spawn,		SHCMD("ameco next") },
  { 0,          XF86XK_AudioPrev,           spawn,		SHCMD("ameco prev") },

// Muting and shits
  { MODKEY,       XK_7,       spawn,		SHCMD("dunstify -t 3000 'Mod + 7' 'Toggle mic'; wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle; pkill -RTMIN+4 dwmblocks") },  // mute mic
  { Mod1Mask,     XK_7,       spawn,		SHCMD("dunstify -t 3000 'Alt + 7' 'Toggle mute';wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle; pkill -RTMIN+4 dwmblocks") },

// repipe

  { ControlMask,  XK_7,       spawn,		SHCMD("repipe") }, //repipe

    { 0,            XF86XK_AudioPlay,           spawn,		SHCMD("ameco toggle") },

// CSGO compatible media hotkeys, will present problems when noclip

  { Mod1Mask,     XK_d,       spawn,		SHCMD("ameco toggle") },
  { Mod1Mask,     XK_a,       spawn,		SHCMD("ameco pause") },
  { Mod1Mask,     XK_v,       spawn,		SHCMD("ameco next") },
  { Mod1Mask,     XK_c,       spawn,		SHCMD("ameco prev") },

// Playback
  { Mod1Mask,     XK_equal,   spawn,		SHCMD("ameco next") },
  { Mod1Mask|ControlMask,  XK_equal,   spawn,		SHCMD("ameco forth") },

  { Mod1Mask,     XK_minus,   spawn,		SHCMD("ameco prev") },
  { Mod1Mask|ControlMask,  XK_minus,   spawn,		SHCMD("ameco back") },

  { MODKEY,       XK_8,       spawn,		SHCMD("ameco select") },
  { Mod1Mask,     XK_8,       spawn,		SHCMD("ameco toggle") },
  { ControlMask,  XK_8,       spawn,		SHCMD("ameco pause") },

  { MODKEY,       XK_9,       spawn,		SHCMD("ameco player") },
  { Mod1Mask,     XK_9,       spawn,		SHCMD("ameco switch") },

// Now we getting into alphabet keys

//  { MODKEY,       XK_m,       spawn,		SHCMD(TERM "lfrun $HOME/.local/bookmarks") },
  { Mod1Mask,     XK_m,       spawn,		SHCMD("mousefix") },
  { ControlMask,  XK_m,       spawn,		SHCMD(TERM "lfrun /media") },

  { MODKEY,       XK_c,       spawn,		SHCMD("classmenu") },
//  { ControlMask,  XK_c,       spawn,		SHCMD(TERM "lfrun $HOME/.local/bookmarks/classes/6") },


  { MODKEY,       XK_apostrophe,       spawn,		SHCMD("wpctl set-volume @DEFAULT_AUDIO_SINK@ 0.3; pkill -RTMIN+4 dwmblocks") },

// Browser
  { MODKEY,                   XK_f,		spawn,		SHCMD("firefox") },
  { MODKEY|ShiftMask,         XK_f,		spawn,		SHCMD("tor-browser") },

  { MODKEY|ShiftMask,	    XK_q,		quit,		{0} },

// F row ft. SHCMD

// Escape aka F0: System components + search
  { MODKEY,       XK_Escape,	spawn,		SHCMD("searchmenu") },
  { ControlMask,  XK_Escape,	spawn,		SHCMD("sysmenu") },
  { Mod1Mask,     XK_Escape,	spawn,		SHCMD("procmenu") },
  { ShiftMask,    XK_Escape,	spawn,		SHCMD("lome") },

// F1: Private
  { MODKEY,       XK_F1,		spawn,		SHCMD("textmenu") },
//  { Mod1Mask,     XK_F1,		spawn,		SHCMD("rustmenu") },
  { ControlMask,  XK_F1,		spawn,		SHCMD("moviemenu") },
  { ShiftMask,    XK_F1,		spawn,		SHCMD("animenu") },

// F2: File/password/clipboard manager
  { MODKEY,       XK_F2,		spawn,		SHCMD(SUDOTERM "lfrun") },
  { Mod1Mask,     XK_F2,		spawn,		SHCMD(TERM "lfrun") },
  { ControlMask,  XK_F2,        spawn,		SHCMD("rbwmenu") },
  { ShiftMask,    XK_F2,        spawn,		SHCMD("clipmenu") },

// F3: GUI shits
  { Mod1Mask,     XK_F3,		spawn,		SHCMD("telegram-desktop") },
  { ControlMask,  XK_F3,        spawn,		SHCMD("standard-notes") },

// F4: Killing shits, just like the number was intended for
  { MODKEY,       XK_F4,		spawn,		SHCMD("dmenu-translate") },
  { Mod1Mask,     XK_F4,		spawn,		SHCMD("killall -9 steam; dunstify 'steam killed' ") },
  { ControlMask,  XK_F4,        spawn,		SHCMD("killall -9 Discord; dunstify 'discord killed' ") },
  { ShiftMask,    XK_F4,		spawn,		SHCMD("rustmenu") },

// F5: Utilities
  { MODKEY,       XK_F5,		spawn,		SHCMD("calmenu") },
  { Mod1Mask,     XK_F5,		spawn,		SHCMD(TERM "newsboat; pkill -RTMIN+15 dwmblocks") },
  { ControlMask,  XK_F5,		spawn,		SHCMD("wordmenu") },
  { ShiftMask,    XK_F5,		spawn,		SHCMD("usbmenu") },

// F6: Gaming
  { MODKEY,       XK_F6,		spawn,		SHCMD("discord") },
  { Mod1Mask,     XK_F6,		spawn,		SHCMD("prime-run steam steam://rungameid/550") },
  { ControlMask,  XK_F6,		spawn,		SHCMD("prime-run steam") },

// F7: Utilities, again
  { MODKEY,       XK_F7,		spawn,		SHCMD("emomenu") },
  { Mod1Mask,     XK_F7,		spawn,		SHCMD("gitmenu") },
  { ControlMask,  XK_F7,		spawn,		SHCMD("histmenu") },

// Screenshot + edit

  { 0,            XK_Print,     spawn,		SHCMD("scrotshot -p") },
  { ShiftMask,    XK_Print,		spawn,		SHCMD("scrotshot -f") },

// Mouse simulator
  { MODKEY,     XK_u,       spawn,		SHCMD("xdotool mousemove 960 100 click 1 && xdotool mousemove 1920 0") }, // click noti
  { MODKEY,		XK_0,       spawn,		SHCMD("xdotool mousemove 1920 0") }, // get out
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          SHCMD("dunstify 'middle click not supported'") }, // midclick status bar
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
