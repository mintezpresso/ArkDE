#ifndef CONFIG_H
#define CONFIG_H

// String used to delimit block outputs in the status.
#define DELIMITER "    "

// Maximum number of Unicode characters that a block can output.
#define MAX_BLOCK_OUTPUT_LENGTH 200

// Control whether blocks are clickable.
#define CLICKABLE_BLOCKS 0

// Control whether a leading delimiter should be prepended to the status.
#define LEADING_DELIMITER 0

// Control whether a trailing delimiter should be appended to the status.
#define TRAILING_DELIMITER 1

// Define blocks for the status feed as X(cmd, interval, signal).
/*    X("sb-anime", 1, 44),
    X("cat $HOME/.cache/playname", 0, 6) */
//    X("cat $HOME/.cache/statusbar/mpv/targetname", 0, 30) \

#define BLOCKS(X)         \
    X("sb-caps", 0, 1) \
    X("sb-mpv", 0, 22) \
    X("sb-spot", 0, 11) \
    X("sb-mpd", 0, 3) \
    X("cat $HOME/.cache/statusbar/xau", 0, 2) \
    X("sb-vol", 0, 4) \
    X("sb-mem", 1, 5) \
    X("sb-temp", 1, 7) \
    X("sb-nettraf", 1, 6) \
    X("cat $HOME/.cache/statusbar/weather", 0, 8) \
    X("sb-net", 5, 9) \
    X("cat $HOME/.cache/statusbar/autoshutdown", 0, 10) \
    X("sb-gas-s", 0, 12) \
    X("sb-pkg", 0, 14) \
    X("sb-news", 0, 15) \
    X("sb-uptime", 60, 19) \
    X("sb-date", 60, 20)

#endif  // CONFIG_H
