/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int exact = 0;						/* -e  option; if 1, dmenu only matches exactly */
static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int fuzzy = 1;                      /* -F  option; if 0, dmenu doesn't use fuzzy matching     */
/* -fn option overrides fonts[0]; default X11 font or font set */

static const char *fonts[] = {
    "Droid:pixelsize=12:antialias=true:autohint=true",
    "NotoCOlorEmoji:pixelsize=11:antialias=true:autohint=true",
    "Joypixels:pixelsize=11:antialias=true:autohint=true",
    "Source Han Code JP:pixelsize=12:antialias=true:autohint=true"
};

static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */

//static const char green[]       = "#39D015";
//static const char yellow[]      = "#fbb901";
//static const char tux_feet[]    = "#f8cb32";
//static const char orange2[]     = "#f7c379";
//static const char red[]         = "#8b0000";
//static const char purple[]      = "#63028c"; // bg

static const char orange[]      = "#ff8700"; // the better orange
static const char cream[]       = "#fae3b0";
static const char blue[]        = "#1e1d2d";
static const char black[]       = "#0f0f0f";
static const char glass[]       = "#23282d";
static const char silver[]      = "#c0c0c0";
static const char purple[]      = "#d700ff"; // fg
static const char white[]       = "#edf2fc";

static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { cream, blue },
	[SchemeSel] = { blue , white },
	[SchemeOut] = { blue , cream },
	[SchemeMid] = { white , blue }, // color of 2 side items
	[SchemeHp]  = { orange , blue }
};

/* -l and -g options; controls number of lines and columns in grid if > 0 */
static unsigned int lines      = 0;
static unsigned int columns    = 0;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
