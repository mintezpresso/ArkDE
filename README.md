## My custom DE/WM built from suckless tools <a href="https://artixlinux.org" ><img src="https://img.shields.io/badge/OS-Artix%20Linux-blue?logo=Artix+Linux" alt="OS: Artix" /></a> <img src="https://img.shields.io/badge/OS-Void%20Linux-295340?logo=Linux" alt="OS: Artix" /></a>

## THIS IS THE DESKTOP BUILD. THERE'S A LAPTOP BUILD IN BRANCH LAPTOP.

- Made for [my dotfiles](https://codeberg.org/mintezpresso/Arktix)
- Designed for keyboard-only navigation, and will mostly use the F row for shortcuts
- Uses ``dash`` shell for fast script execution

**Testing**
- Tested on Artix, Void, Debian base, Gecko Rolling barebones

**Other resources**
- Scripts: scripts used in dwm can be found [here](https://codeberg.org/mintezpresso/scripts)

### Install
```
cd

git clone https://github.com/mintezpresso/ArkDE

./install.sh
```
