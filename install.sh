#!/bin/sh
# Install script for ArkDE, as a suckless build
# mintezpresso

case $(whoami) in
    root | artix ) printf "Root detected. Enter human user to install ArkDE for: " && read username && printf "Now installing ArkDE for "$username"\n" ;;
    * ) username=$(whoami) && printf "Now installing ArkDE for "$username"\n" ;;
esac

mkdir -p /home/$username/.local/src
git -c /home/$username/.local/src clone https://codeberg.org/mintezpresso/arkde

printf "\nInstalling dwm\n"
cd /home/"$username"/.local/src/arkde/dwm
sudo make clean install
printf "\ndwm done"
sleep 2

printf "\nInstalling st\n"
cd /home/"$username"/.local/src/arkde/st
sudo make clean install
printf "\nst done\n"
sleep 2

printf "\nInstalling dmenu\n"
cd /home/"$username"/.local/src/arkde/dmenu
sudo make clean install
printf "\ndmenu done\n"
sleep 2

printf "\nInstalling dwmblocks\n"
cd /home/"$username"/.local/src/arkde/blocks
sudo make clean install
printf "\ndwmblocks done\n"
sleep 2

printf "Finished installing ArkDE\n"
