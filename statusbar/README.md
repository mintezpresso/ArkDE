# Scripts for dwmblocks
Most of these are complete and is very likely to NOT get anymore updates

- [sb-anime](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/sb-anime): module for [animenu](https://codeberg.org/mintezpresso/Scripts/src/branch/main/media/animenu)
- [sb-autoshutdown](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/sb-autoshutdown): show shutdown time provided by [sysmenu](https://codeberg.org/mintezpresso/Scripts/src/branch/main/admin/sysmenu) option 6
- [sb-cal](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/sb-cal): module for [calmenu](https://codeberg.org/mintezpresso/Scripts/src/branch/main/dmenu/calmenu): shows reminders due within 1 week from current date
- [sb-capslock](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/sb-capslock): capslock indicator. needs update
- [sb-connman](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/sb-connman): internet indicator
- [sb-date](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/sb-date): shows current date and time
- [sb-gas](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/sb-gas): Vietnamese gas price
- [sb-memory](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/sb-memory): RAM usage
- [sb-mpd](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/sb-mpd): current mpd playback status
- [sb-mpdup](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/sb-mpdup): updates sb-mpd
- [sb-mpv](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/sb-mpv): mpv's current playback status. fully compatible with sb-anime
- [sb-nettraf](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/sb-nettraf): current internet speed
- [sb-spotify](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/sb-spotify): display current ncspot item
- [sb-storage](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/sb-storage): storage info for /home (in case of separate /home partition)
- [sb-temp](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/sb-temp): current CPU temperature
- [sb-uptime](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/sb-uptime): time since boot
- [sb-volume](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/sb-volume): volume from pipewire


### Inactive scripts (either I don't use them or they're not worked on)
- [sb-cmus](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/.inactive/sb-cmus): cmus playback info
- [sb-coin](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/.inactive/sb-coin): crypto prices
- [sb-forex](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar/.inactive/sb-forex): forex prices
