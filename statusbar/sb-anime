#!/bin/sh
# Display current mpv playback info to dwmblocks, designed for your anime folder
# Written for animenu (https://codeberg.org/mintezpresso/Scripts/src/branch/main/media/animenu)
# mintezpresso

# Example output:  jashin-chan season 2 ep 8/12 |  19:20/23:39

# If this thing prints movie when playing a normal ep, sudo rm /tmp/mpvsocket, then reopen anime from animenu
# Will be fixed in a later update. eta unknown

. $HOME/.config/mediamenu/anime_config
# Sample: https://codeberg.org/mintezpresso/Arktix/src/branch/main/config/mediamenu/anime_config

# Dir example with "/media/anime" as $animedir:
# /media/anime/jashin-chan/2/8.mp4
# /media/anime/vivy/1.mp4

[ -z "$(pgrep -x animenu)" ] && exit

POSITION=$(echo '{ "command": ["get_property_string", "time-pos"] }' | socat - /tmp/mpvsocket | jq '.data | tonumber | floor')
DURATION=$(echo '{ "command": ["get_property_string", "duration"] }' | socat - /tmp/mpvsocket | jq '.data | tonumber | floor')
IDLE=$(echo '{ "command": ["get_property", "core-idle"] }' | socat - /tmp/mpvsocket | jq .data | sed -e 's/\ $//;s/\"//g' )
EPISODEVAR=$(echo '{ "command": ["get_property", "filename/no-ext"] }' | socat - /tmp/mpvsocket | jq .data | sed -e 's/ //;s/\"//g' )
EPCOUNTVAR=$(echo '{ "command": ["get_property", "playlist/count"] }' | socat - /tmp/mpvsocket | jq .data | sed -e 's/ //;s/\"//g' )
PATHNAME=$(dirname $(echo '{ "command": ["get_property", "path"] }' | socat - /tmp/mpvsocket | jq .data | sed s/\"//g))

printfunc () {

case $EPISODEVAR in
    [0-9]|[1-9][0-9]* ) EPISODE=$(printf "ep $EPISODEVAR") && EPCOUNT=$(printf "/$EPCOUNTVAR") ;;
    * ) EPISODE="$EPISODEVAR" && EPCOUNT="" ;;
esac

[ "$(dirname $(dirname $PATHNAME))" = "$animedir" ] \
    && ANIMENAME=$(basename $(dirname $PATHNAME) | sed 's/_/ /g') && SEASON=$(printf " $(basename $PATHNAME | sed 's/_/ /g')") \
    || ANIMENAME=$(basename $PATHNAME | sed 's/_/ /g')

[ "$IDLE" = "true" ] && STATUS=" " || STATUS=" "

printf "🎬 "
printf "$ANIMENAME$SEASON $EPISODE$EPCOUNT |$STATUS "

if [ "$(printf $((DURATION/3600)))" = "0" ]; then
    printf "%0d:%02d/" $((POSITION%3600/60)) $((POSITION%60))
    printf "%0d:%02d" $((DURATION%3600/60)) $((DURATION%60))
else
    printf "%0d:%0d:%02d/" $((POSITION/3600)) $((POSITION%3600/60)) $((POSITION%60))
    printf "%0d:%0d:%02d" $((DURATION/3600)) $((DURATION%3600/60)) $((DURATION%60))
fi
}

[ -z "$mpvpid" ] && [ -n "$animenupid" ] \
    && (case "$(tail -n 1 /tmp/animenu.key)" in
            "zz" ) printf "  animenu: viewing librarry" ;;
            "xx" ) printf "  animenu: editing script" ;;
            "Search" ) printf "  animenu: looking for new anime in browser" ;;
            "Browse" ) printf "  animenu: browsing local collection" ;;
        esac) \
    || printfunc
